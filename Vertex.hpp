
#ifndef _VERTEX_H_
#define _VERTEX_H_

#include <GL/gl.h>

class Vertex
{
public:
  Vertex(GLfloat x = 0.f, GLfloat y = 0.f, GLfloat z = 0.f,
         GLfloat r = 1.f, GLfloat g = 1.f, GLfloat b = 1.f,
         GLfloat nx = 0.f, GLfloat ny = 0.f, GLfloat nz = 0.f);

  GLfloat distance(const Vertex& other) const;

public:
  GLfloat x, y, z;
  GLfloat r, g, b;
  GLfloat nx, ny, nz;
};

#endif /* _VERTEX_H_ */
