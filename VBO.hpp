#ifndef _VBO_H_
#define _VBO_H_

#include <vector>

#include <GL/gl.h>

struct DataInfo;


class VBO
{
public:
  static void BindNull(GLenum target);

public:
  VBO(GLenum target = GL_ARRAY_BUFFER);
  ~VBO();

  void create();
  void destroy();
  void allocate(GLuint size, GLenum usage = GL_STATIC_DRAW);

  GLuint getIdentifier() const;
  GLenum getTarget() const;
  GLuint getSize() const;
  size_t getDataInfoCount() const;
  const DataInfo& getDataInfo(size_t n) const;

  void bind() const;
  void appendData(const std::vector<GLfloat>& data);
  void appendData(const GLfloat* const data, size_t count);
  void addDataInfo(DataInfo info);

private:
  void appendDataThrowIfError() const;

private:
  GLenum target;
  GLuint identifier;

  std::vector<DataInfo> infos;

  GLuint allocatedSize;
  GLuint size;
};

#endif /* _VBO_H_ */
