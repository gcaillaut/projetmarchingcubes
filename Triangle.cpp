#include <glm/glm.hpp>

#include "Triangle.hpp"


Triangle::Triangle(Vertex a, Vertex b, Vertex c):
vertices {{ a, b, c }}
{
  auto u = glm::vec3(b.x - a.x, b.y - a.y, b.z - a.z);
  auto v = glm::vec3(c.x - a.x, c.y - a.y, c.z - a.z);

  auto nx(u.y * v.z - u.z * v.y),
    ny(u.z * v.x - u.x * v.z),
    nz(u.x * v.y - u.y * v.x);

  for (auto& v : vertices)
    {
      v.nx = nx;
      v.ny = ny;
      v.nz = nz;
    }
}
