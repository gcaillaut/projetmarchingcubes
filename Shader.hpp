#ifndef _SHADER_H_
#define _SHADER_H_

#include <string>

#include <glm/gtc/type_ptr.hpp>


class Shader
{
public:
  static void resetUsedProgram();

public:
  Shader(const std::string& vertexFilename, const std::string& fragmentFilename);
  ~Shader();

  void useProgram() const;

  GLuint getUniformLocation(const std::string& name) const;

  void setUniformMat4(GLuint location, const glm::mat4& matrix)
  {
    glUniformMatrix4fv(location, 1, GL_FALSE, glm::value_ptr(matrix));
  }

  void setUniformVec3(GLuint location, const glm::vec3& vec)
  {
    glUniform3fv(location, 1, glm::value_ptr(vec));
  }

  void setUniformFloat(GLuint location, const GLfloat value)
  {
    glUniform1f(location, value);
  }

private:
  static std::string readFile(const std::string& filepath);
  static void checkCompileError(const GLuint shaderId);
  static void checkProgramError(const GLuint programId);

private:
  GLuint programIdentifier;
};

#endif /* _SHADER_H_ */
