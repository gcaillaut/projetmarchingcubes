# Projet OpenGL
## Gaëtan Caillaut



### Commandes

`Entrer` Passer du mode polygone au mode fil de fer

`z`, `q`, `s`, `d` Déplacer la caméra

`Clic gauche + mouvement souris` Rotation de la caméra


### Makefile

`make` Compiler

`make run` Lancer le programme

`make record` Enregistrer une vidéo

`make video` Lire une vidéo
