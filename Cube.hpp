#ifndef _CUBE_H_
#define _CUBE_H_

#include <vector>

#include "BBox3.hpp"
#include "Triangle.hpp"

class VBO;


class Cube
{
public:
  Cube(const BBox3<GLfloat>& bbox);

  VBO buildVBO() const;
  const BBox3<GLfloat>& getBBox() const;

public:
  BBox3<GLfloat> bbox;
  std::vector<Triangle> triangles;
};

#endif /* _CUBE_H_ */
