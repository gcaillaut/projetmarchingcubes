#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>

#include "MoleculeLoader.hpp"
#include "Molecule.hpp"


Molecule MoleculeLoader::loadFromFile(const std::string& filepath) const
{
  std::vector<Atom> atoms;
  std::ifstream file(filepath);
  std::string line;

  // On saute la première ligne
  std::getline(file, line);

  while (std::getline(file, line) && line != "END")
    {
      std::stringstream ss(line);
      ss.ignore(12);

      std::string name;
      ss >> name;

      ss.ignore(15);
      GLfloat x, y, z, radius;
      ss >> x >> y >> z >> radius;
      atoms.emplace_back(x, y, z, radius, name);
    }
  file.close();

  return Molecule(atoms);
}
