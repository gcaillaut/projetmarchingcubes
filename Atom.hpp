#ifndef _ATOM_H_
#define _ATOM_H_

#include "Vertex.hpp"


struct Atom
{
  Atom() = default;
  Atom(GLfloat x, GLfloat y, GLfloat z, GLfloat radius, const std::string& name = ""):
    position(x, y, z), radius(radius), name(name)
  { }

  Vertex position;
  GLfloat radius;
  std::string name;
};

#endif /* _ATOM_H_ */
