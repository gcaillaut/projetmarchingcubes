#include <iostream>
#include "Molecule.hpp"

  Molecule::Molecule(const std::vector<Atom>& atoms):
    atoms(atoms), isBBoxComputed(false)
  { }


  Molecule::Molecule(std::vector<Atom>&& atoms):
    atoms(std::move(atoms)), isBBoxComputed(false)
  { }

BBox3<GLfloat>& Molecule::getBBox() const
  {
    if (!isBBoxComputed)
      {
        GLfloat xmin, xmax, ymin, ymax, zmin, zmax;
        xmin = xmax = atoms.at(0).position.x;
        ymin = ymax = atoms.at(0).position.y;
        zmin = zmax = atoms.at(0).position.z;

        for (const auto& a : atoms)
          {
            xmin = std::min(xmin, a.position.x - a.radius);
            xmax = std::max(xmax, a.position.x + a.radius);

            ymin = std::min(ymin, a.position.y - a.radius);
            ymax = std::max(ymax, a.position.y + a.radius);

            zmin = std::min(zmin, a.position.z - a.radius);
            zmax = std::max(zmax, a.position.z + a.radius);
          }

        bbox.xmin = xmin;
        bbox.xmax = xmax;
        bbox.ymin = ymin;
        bbox.ymax = ymax;
        bbox.zmin = zmin;
        bbox.zmax = zmax;

        isBBoxComputed = true;
      }
    return bbox;
  }
