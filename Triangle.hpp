#ifndef _TRIANGLE_H_
#define _TRIANGLE_H_

#include <array>

#include "Vertex.hpp"


class Triangle
{
public:
  Triangle(Vertex a, Vertex b, Vertex c);

public:
  std::array<Vertex, 3> vertices;
};

#endif /* _TRIANGLE_H_ */
