#include <cmath>

#include "Vertex.hpp"


Vertex::Vertex(GLfloat x, GLfloat y, GLfloat z,
               GLfloat r, GLfloat g, GLfloat b,
               GLfloat nx, GLfloat ny, GLfloat nz):
  x(x), y(y), z(z),
  r(r), g(g), b(b),
  nx(nx), ny(ny), nz(nz)
  { }


GLfloat Vertex::distance(const Vertex& other) const
{
  const auto xDiff(x - other.x),
    yDiff(y - other.y),
    zDiff(z - other.z);

  return std::sqrt(xDiff*xDiff + yDiff*yDiff + zDiff*zDiff);
}
