#ifndef _TEXTURELOADER_H_
#define _TEXTURELOADER_H_

#include <GL/gl.h>


class TextureLoader
{
public:
  virtual ~TextureLoader() {}

  virtual GLuint loadFromFile(const std::string& filepath) const = 0;
};

#endif /* _TEXTURELOADER_H_ */
