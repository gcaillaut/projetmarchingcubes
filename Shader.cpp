#include <iostream>
#include <fstream>
#include <stdexcept>
#include <sstream>
#include <string>
#include <vector>

#include <GL/glew.h>

#include "Shader.hpp"


void Shader::resetUsedProgram()
{
  glUseProgram(0);
}


Shader::Shader(const std::string& vertexFilename, const std::string& fragmentFilename)
{
  GLuint vertexIdentifier = glCreateShader(GL_VERTEX_SHADER),
    fragmentIdentifier = glCreateShader(GL_FRAGMENT_SHADER);

  std::string vertexCode(Shader::readFile(vertexFilename)),
    fragmentCode(Shader::readFile(fragmentFilename));

  auto vertexBuffer(vertexCode.c_str()), fragmentBuffer(fragmentCode.c_str());
  glShaderSource(vertexIdentifier, 1, &vertexBuffer, nullptr);
  glShaderSource(fragmentIdentifier, 1, &fragmentBuffer, nullptr);

  glCompileShader(vertexIdentifier);
  std::cout << "Checking error for vertex shader (" << vertexFilename << "):" << std::endl;
  Shader::checkCompileError(vertexIdentifier);

  glCompileShader(fragmentIdentifier);
  std::cout << "Checking error for fragment shader (" << fragmentFilename << ")" << std::endl;
  Shader::checkCompileError(fragmentIdentifier);

  programIdentifier = glCreateProgram();
  glAttachShader(programIdentifier, vertexIdentifier);
  glAttachShader(programIdentifier, fragmentIdentifier);
  glLinkProgram(programIdentifier);

  glDetachShader(programIdentifier, vertexIdentifier);
  glDetachShader(programIdentifier, fragmentIdentifier);

  glDeleteShader(vertexIdentifier);
  glDeleteShader(fragmentIdentifier);
}


Shader::~Shader()
{
  glDeleteProgram(programIdentifier);
}


void Shader::useProgram() const
{
  glUseProgram(programIdentifier);
}


std::string Shader::readFile (const std::string& filepath)
{
  std::string code;
  std::ifstream file(filepath);
  if (file.is_open())
    {
      std::string line;
      while (std::getline(file, line))
        {
          code += line + '\n';
        }
    }
  else
    {
      throw std::runtime_error("Cannot load file " + filepath);
    }

  return code;
}


GLuint Shader::getUniformLocation(const std::string& name) const
{
  return glGetUniformLocation(programIdentifier, name.c_str());
}


void Shader::checkCompileError(const GLuint shaderId)
{
  GLint result(GL_FALSE);
  int infoLogLength;

    auto err(glGetError());
  if (err != GL_NO_ERROR)
    {
      std::stringstream ss;
      ss << "Error: " << gluErrorString(err) << std::endl;
      throw std::runtime_error(ss.str());
    }

  glGetShaderiv(shaderId, GL_COMPILE_STATUS, &result);
  glGetShaderiv(shaderId, GL_INFO_LOG_LENGTH, &infoLogLength);

  if (infoLogLength > 1)
    {
      std::vector<char> shaderErrorMessage(infoLogLength + 1);
      glGetShaderInfoLog(shaderId, infoLogLength, nullptr, shaderErrorMessage.data());
      std::cout << shaderErrorMessage.data() << std::endl;
    }
  else
    {
      std::cout << "No error." << std::endl;
    }
}


void Shader::checkProgramError(const GLuint programId)
{
  GLint result(GL_FALSE);
  int infoLogLength;

  glGetProgramiv(programId, GL_LINK_STATUS, &result);
  glGetProgramiv(programId, GL_INFO_LOG_LENGTH, &infoLogLength);

  if (infoLogLength > 0)
    {
      std::vector<char> programErrorMessage(infoLogLength + 1);
      glGetProgramInfoLog(programId, infoLogLength, nullptr, programErrorMessage.data());
      std::cout << programErrorMessage.data() << std::endl;
    }
}
