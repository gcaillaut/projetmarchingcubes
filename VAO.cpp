#include <iostream>
#include <stdexcept>

#include <GL/glew.h>

#include "VAO.hpp"
#include "VBO.hpp"
#include "DataInfo.hpp"


void VAO::BindNull()
{
  glBindVertexArray(0);
}


VAO::VAO()
{
  glGenVertexArrays(1, &identifier);
}


VAO::~VAO()
{
  if (glIsVertexArray(identifier) == GL_TRUE)
    {
      glDeleteVertexArrays(1, &identifier);
    }
}


void VAO::create()
{
  if (glIsVertexArray(identifier) == GL_TRUE)
    {
      throw std::runtime_error("VAO is already created.");
    }
  glGenVertexArrays(1, &identifier);
}


void VAO::destroy()
{
  if (glIsVertexArray(identifier) == GL_FALSE)
    {
      throw std::runtime_error("VBO isn't created, cannot destroy it.");
    }
  glDeleteVertexArrays(1, &identifier);
  identifier = 0u;
}


void VAO::bind() const
{
  glBindVertexArray(identifier);
}


void VAO::beginSetup() const
{
  bind();
}


void VAO::endSetup() const
{
  VAO::BindNull();
  VBO::BindNull(GL_ARRAY_BUFFER);
}


void VAO::setupVBO(const VBO& vbo) const
{
  auto count(vbo.getDataInfoCount());

  vbo.bind();

  for (auto i(0u) ; i < count ; ++i)
    {
      auto& info(vbo.getDataInfo(i));
      glEnableVertexAttribArray(info.index);
      glVertexAttribPointer(info.index,
                            info.size,
                            info.type,
                            info.normalized,
                            info.stride,
                            info.pointer);
    }
}
