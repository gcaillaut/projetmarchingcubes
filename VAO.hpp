#ifndef _VAO_H_
#define _VAO_H_

#include <GL/gl.h>

class VBO;


class VAO
{
public:
  static void BindNull();

public:
  VAO();
  ~VAO();

  void create();
  void destroy();

  void bind() const;

  void beginSetup() const;
  void endSetup() const;
  void setupVBO(const VBO& vbo) const;

private:
  GLuint identifier;
};

#endif /* _VAO_H_ */
