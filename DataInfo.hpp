#ifndef _DATAINFO_H_
#define _DATAINFO_H_

#include <GL/gl.h>


struct DataInfo
{
  DataInfo():
    index(0u), size(3), type(GL_FLOAT), normalized(GL_FALSE), stride(0), pointer(0)
  { }

  GLuint index;
  GLint size;
  GLenum type;
  GLboolean normalized;
  GLsizei stride;
  const GLvoid* pointer;
};

#endif /* _DATAINFO_H_ */
