#ifndef _COLLISIONINFO_H_
#define _COLLISIONINFO_H_

#include <array>

#include <GL/gl.h>

#include "Vertex.hpp"


struct CollisionInfo
{
  CollisionInfo():
    edgeFlags(0)
  { }

  GLint edgeFlags;
  std::array<Vertex, 8> centers;
};

#endif /* _COLLISIONINFO_H_ */
