#include <array>
#include <fstream>
#include <stdexcept>
#include <vector>

#include "BMPLoader.hpp"


GLuint BMPLoader::loadFromFile(const std::string& filepath) const
{
  constexpr size_t HEADER_SIZE = 54;
  std::array<char, HEADER_SIZE> header;
  unsigned int dataPos;
  unsigned int width, height, size;

  std::ifstream file(filepath, std::ios::binary);
  if (!file.is_open())
    {
      throw std::runtime_error("Cannot open file: " + filepath);
    }

  if (file.readsome(header.data(), HEADER_SIZE) != HEADER_SIZE
      || header[0] != 'B'
      || header[1] != 'M')
    {
      throw std::runtime_error("File: " + filepath + " is not a valid BMP file.");
    }

  dataPos = *(unsigned int*) &(header[0x0A]);
  size = *(unsigned int*) &(header[0x22]);
  width = *(unsigned int*) &(header[0x12]);
  height = *(unsigned int*) &(header[0x16]);

  if (size == 0)
    {
      size = width * height * 3u;
    }
  if (dataPos == 0)
    {
      dataPos = 54u;
    }

  std::vector<char> data(size);
  // file.seekg(std::ios_base::beg);
  file.read(data.data(), size);

  GLuint textureIdentifier;
  glGenTextures(1, &textureIdentifier);

  glBindTexture(GL_TEXTURE_2D, textureIdentifier);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_BGR, GL_UNSIGNED_BYTE, data.data());
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

  return textureIdentifier;
}
