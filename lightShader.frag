#version 330 core

in vec3 fragmentPosition;
in vec3 fragmentColor;
in vec3 normal;

uniform float ambientLightIntensity;
uniform vec3 ambientLightColor;

uniform float diffuseLightIntensity;
uniform vec3 diffuseLightPosition;
uniform vec3 diffuseLightColor;

uniform float specularLightIntensity;
uniform vec3 eyePosition;

out vec4 color;

void main()
{
  vec3 ambientLight = ambientLightIntensity * ambientLightColor;

  vec3 norm = normalize(normal);
  vec3 lightDirection = normalize(diffuseLightPosition - fragmentPosition);
  float diff = max(dot(norm, lightDirection), 0.0);
  vec3 diffuseLight = diffuseLightIntensity * diff * diffuseLightColor;

  vec3 viewDirection = normalize(eyePosition - fragmentPosition);
  vec3 reflectDirection = reflect(-lightDirection, norm);
  float spec = pow(max(dot(viewDirection, reflectDirection), 0.0), 128);
  vec3 specularLight = specularLightIntensity * spec * diffuseLightColor;

  color = vec4((ambientLight + diffuseLight + specularLight) * fragmentColor, 1.0);
}
