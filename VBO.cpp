#include <stdexcept>
#include <sstream>

#include <GL/glew.h>

#include "VBO.hpp"
#include "DataInfo.hpp"


void VBO::BindNull(GLenum target)
{
  glBindBuffer(target, 0u);
}


VBO::VBO(GLenum target):
  target(target), allocatedSize(0u), size(0)
{
  glGenBuffers(1, &identifier);
}


VBO::~VBO()
{
  if (glIsBuffer(identifier) == GL_TRUE)
    {
      glDeleteBuffers(1, &identifier);
    }
}


void VBO::create()
{
  if (glIsBuffer(identifier) == GL_TRUE)
    {
      throw std::runtime_error("VBO is already created.");
    }
  glGenBuffers(1, &identifier);
}


void VBO::destroy()
{
  if (glIsBuffer(identifier) == GL_FALSE)
    {
      throw std::runtime_error("VBO isn't created, cannot destroy it.");
    }
  glDeleteBuffers(1, &identifier);
  infos.clear();
  identifier = 0u;
}


void VBO::allocate(GLuint size, GLenum usage)
{
  bind();
  glBufferData(target, size * sizeof(GLfloat), nullptr, usage);

  auto err(glGetError());
  if (err == GL_NO_ERROR)
    {
      allocatedSize = size;
    }
  else
    {
      std::stringstream ss;
      ss << "Error when allocating VBO " << identifier << ": " << gluErrorString(err) << std::endl;
      throw std::runtime_error(ss.str());
    }

  VBO::BindNull(target);
}


GLuint VBO::getIdentifier() const
{
  return identifier;
}


GLenum VBO::getTarget() const
{
  return target;
}


GLuint VBO::getSize() const
{
  return size;
}


size_t VBO::getDataInfoCount() const
{
  return infos.size();
}


const DataInfo& VBO::getDataInfo(size_t n) const
{
  return infos.at(n);
}


void VBO::bind() const
{
  glBindBuffer(target, identifier);
}


void VBO::appendData(const std::vector<GLfloat>& data)
{
  GLintptr offsetInBytes(size * sizeof(GLfloat));
  GLsizeiptr sizeInBytes(data.size() * sizeof(GLfloat));

  bind();

  glBufferSubData(target, offsetInBytes, sizeInBytes, data.data());

  appendDataThrowIfError();
  size += data.size();

  VBO::BindNull(target);
}


void VBO::appendData(const GLfloat* const data, size_t count)
{
  GLintptr offsetInBytes(size * sizeof(GLfloat));
  GLsizeiptr sizeInBytes(count * sizeof(GLfloat));

  bind();

  glBufferSubData(target, offsetInBytes, sizeInBytes, data);

    appendDataThrowIfError();
    size += count;

  VBO::BindNull(target);
}


void VBO::addDataInfo(DataInfo info)
{
  infos.push_back(std::move(info));
}


void VBO::appendDataThrowIfError() const
{
  auto err(glGetError());
  if (err != GL_NO_ERROR)
    {
      std::stringstream ss;
      ss << "Error when adding data in VBO " << identifier << ": " << gluErrorString(err) << std::endl;
      throw std::runtime_error(ss.str());
    }
}
