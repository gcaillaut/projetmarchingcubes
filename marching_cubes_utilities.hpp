#ifndef MARCHING_HPP
#define MARCHING_HPP

#include <vector>

#include <GL/glut.h>
#include <GL/gl.h>

#include "BBox3.hpp"

struct CollisionInfo;


GLvoid vMarchCube(int iFlagIndex, GLfloat* points, std::vector<GLfloat>* LesTriangles);
std::vector<GLfloat> vMarchCube(const CollisionInfo& info, const BBox3<GLfloat>& points);

#endif
