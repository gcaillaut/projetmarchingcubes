#include "Cube.hpp"
#include "DataInfo.hpp"
#include "VBO.hpp"
#include "Vertex.hpp"


constexpr size_t vertexPerTriangle(3), trianglePerSide(2), sidePerCube(6);

Cube:: Cube(const BBox3<GLfloat>& bbox):
  bbox(bbox)
{
  triangles.reserve(vertexPerTriangle * trianglePerSide * sidePerCube);

  Vertex
    a(bbox.xmin, bbox.ymin, bbox.zmin),
    b(bbox.xmax, bbox.ymin, bbox.zmin),
    c(bbox.xmax, bbox.ymax, bbox.zmin),
    d(bbox.xmin, bbox.ymax, bbox.zmin),

    e(bbox.xmin, bbox.ymin, bbox.zmax),
    f(bbox.xmax, bbox.ymin, bbox.zmax),
    g(bbox.xmax, bbox.ymax, bbox.zmax),
    h(bbox.xmin, bbox.ymax, bbox.zmax);

  // Back side
  // triangles.emplace_back(a, b, c);
  // triangles.emplace_back(a, c, d);
  triangles.emplace_back(b, a, d);
  triangles.emplace_back(b, d, c);

  // Front side
  triangles.emplace_back(e, f, g);
  triangles.emplace_back(e, g, h);

  // Top side
  triangles.emplace_back(h, g, c);
  triangles.emplace_back(h, c, d);

  // Bottom side
  // triangles.emplace_back(e, f, b);
  // triangles.emplace_back(e, b, a);
  triangles.emplace_back(a, b, f);
  triangles.emplace_back(a, f, e);

  // Left side
  triangles.emplace_back(a, e, h);
  triangles.emplace_back(a, h, d);

  // Right side
  triangles.emplace_back(f, b, c);
  triangles.emplace_back(f, c, g);
}


VBO Cube::buildVBO() const
{
  VBO vbo;
  constexpr size_t totalSize = vertexPerTriangle * trianglePerSide * sidePerCube;

  vbo.allocate(totalSize * (4u * 3u));
  vbo.appendData(reinterpret_cast<const GLfloat*>(triangles.data()), totalSize * (3u + 3u + 3u));

  DataInfo info;
  info.stride = sizeof(Vertex);
  vbo.addDataInfo(info);

  DataInfo colorInfo;
  colorInfo.index = 1;
  colorInfo.stride = sizeof(Vertex);
  colorInfo.pointer = reinterpret_cast<const GLvoid*>(3 * sizeof(GLfloat));
  vbo.addDataInfo(colorInfo);

  DataInfo normalInfo;
  normalInfo.index = 2;
  normalInfo.stride = sizeof(Vertex);
  normalInfo.pointer = reinterpret_cast<const GLvoid*>(2 * 3 * sizeof(GLfloat));
  vbo.addDataInfo(normalInfo);

  return vbo;
}


const BBox3<GLfloat>& Cube::getBBox() const
{
  return bbox;
}
