CXX=clang++
CXXFLAGS=--std=c++11 -Wall -Wextra -O2 -Os
LDFLAGS=-lGL -lglfw -lGLEW -lGLU
SRC=$(wildcard *.cpp)
OBJ=$(SRC:.cpp=.o)
EXEC=exec

all: $(EXEC)

$(EXEC): $(OBJ)
	$(CXX) -o $@ $^ $(LDFLAGS)

%.o: %.cpp
	$(CXX) -o $@ -c $^ $(CXXFLAGS)

.PHONY: clean mrproper

clean:
	rm *.o

mrproper:
	rm *.o $(EXEC) $(EXEC).trace

run: $(EXEC)
	./$(EXEC) model.pdb 100

record: $(EXEC)
	LD_PRELOAD=apitrace/wrappers/glxtrace.so ./$(EXEC) model.pdb 100

video:
	apitrace/glretrace $(EXEC).trace
