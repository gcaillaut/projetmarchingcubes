#version 330 core

layout(location = 0) in vec3 vertexPosition_modelspace;
layout(location = 1) in vec3 vertex_color;
layout(location = 2) in vec3 vertex_normal;

uniform mat4 MVP;

out vec3 fragmentPosition;
out vec3 fragmentColor;
out vec3 normal;

void main()
{
  // vec3 lightPosition = vec3(10, 10, 10);
  // float cosTheta = clamp(dot(normal_modelspace, lightPosition), 0 , 1);

  gl_Position = MVP * vec4(vertexPosition_modelspace, 1.0);
  fragmentColor = vertex_color;
  normal = vertex_normal;
  fragmentPosition = vertexPosition_modelspace;
}
