#ifndef _MOLECULE_H_
#define _MOLECULE_H_

#include <vector>

#include <GL/gl.h>

#include "Atom.hpp"
#include "BBox3.hpp"


class Molecule
{
public:
  Molecule(const std::vector<Atom>& atoms);
  Molecule(std::vector<Atom>&& atoms);

  BBox3<GLfloat>& getBBox() const;

public:
  std::vector<Atom> atoms;
  mutable BBox3<GLfloat> bbox;
  mutable bool isBBoxComputed;
};

#endif /* _MOLECULE_H_ */
