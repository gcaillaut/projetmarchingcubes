#ifndef _PDBLOADER_H_
#define _PDBLOADER_H_

class PDBLoader
{
public:
  void loadFromFile(const std::string& filename) const;
};

#endif /* _PDBLOADER_H_ */
