#ifndef _MOLECULELOADER_H_
#define _MOLECULELOADER_H_

class Molecule;


class MoleculeLoader
{
public:
  Molecule loadFromFile(const std::string& filepath) const;
};

#endif /* _MOLECULELOADER_H_ */
