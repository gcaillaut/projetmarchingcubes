#ifndef _BMPLOADER_H_
#define _BMPLOADER_H_

#include "TextureLoader.hpp"


class BMPLoader: public TextureLoader
{
public:
  virtual GLuint loadFromFile(const std::string& filepath) const;
};

#endif /* _BMPLOADER_H_ */
