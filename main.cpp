#include <algorithm>
#include <array>
#include <fstream>
#include <iterator>
#include <iostream>
#include <functional>
#include <sstream>
#include <string>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtx/transform.hpp>

#include "BMPLoader.hpp"
#include "Shader.hpp"

#include "Atom.hpp"
#include "CollisionInfo.hpp"
#include "Cube.hpp"
#include "DataInfo.hpp"
#include "Molecule.hpp"
#include "MoleculeLoader.hpp"
#include "Vertex.hpp"
#include "VAO.hpp"
#include "VBO.hpp"

#include "marching_cubes_utilities.hpp"


void windowSizeCallback(GLFWwindow* window, int width, int height)
{
  glViewport(0, 0, width, height);
}


void setColor(Vertex& vertex, const std::string& name)
{
  if (name.find("H") != std::string::npos)
    {
      vertex.r = 1.f;
      vertex.g = 1.f;
      vertex.b = 1.f;
    }
  else if (name.find("C") != std::string::npos)
    {
      vertex.r = 0.f;
      vertex.g = 0.f;
      vertex.b = 0.f;
    }
  else if (name.find("N") != std::string::npos)
    {
      vertex.r = .5f;
      vertex.g = .8f;
      vertex.b = .92f;
    }
  else if (name.find("O") != std::string::npos)
    {
      vertex.r = 1.0f;
      vertex.g = .0f;
      vertex.b = .0f;
    }
  else
    {
      vertex.r = 0.f;
      vertex.g = 1.f;
      vertex.b = 0.f;
    }
}


/**
 * \brief Subdivise une BBox.
 *
 * \param bbox La bbox.
 * \param xCount Le nombre de subdivisions en x
 * \param yCount Le nombre de subdivisions en y
 * \param zCount Le nombre de subdivisions en z
 */
std::vector<BBox3<GLfloat>> subDivideBBox(const BBox3<GLfloat> bbox, const size_t xCount, const size_t yCount, const size_t zCount)
{
  const auto COUNT(xCount + yCount + zCount);
  auto WIDTH(bbox.getWidth() / xCount),
    HEIGHT(bbox.getHeight() / yCount),
    DEPTH(bbox.getDepth() / zCount);

  std::vector<BBox3<GLfloat>> res;
  res.reserve(COUNT);

  for (auto x(0u) ; x < xCount ; ++x)
    {
      const auto xmin(bbox.xmin + x * WIDTH);
      for (auto y(0u) ; y < yCount ; ++y)
        {
          const auto ymin(bbox.ymin + y * HEIGHT);
          for (auto z(0u) ; z < zCount ; ++z)
            {
              const auto zmin(bbox.zmin + z * DEPTH);
              res.emplace_back(xmin, xmin + WIDTH,
                               ymin, ymin + HEIGHT,
                               zmin, zmin + DEPTH);
            }
        }
    }

  return res;
}

/**
 * \brief Returns the cubeEdgeFlag value of the marching.
 *
 * \param bbox La partie du cube traitée.
 * \param molecule La molecule à afficher
 */
CollisionInfo cubeEdgeFlagFromCollision(BBox3<GLfloat> bbox, const Molecule& molecule)
{
  constexpr size_t VERTEX_COUNT = 8;
  const Vertex
    backBottomLeft(bbox.xmin, bbox.ymin, bbox.zmin),
    backBottomRight(bbox.xmax, bbox.ymin, bbox.zmin),
    backTopRight(bbox.xmax, bbox.ymax, bbox.zmin),
    backTopLeft(bbox.xmin, bbox.ymax, bbox.zmin),

    frontBottomLeft(bbox.xmin, bbox.ymin, bbox.zmax),
    frontBottomRight(bbox.xmax, bbox.ymin, bbox.zmax),
    frontTopRight(bbox.xmax, bbox.ymax, bbox.zmax),
    frontTopLeft(bbox.xmin, bbox.ymax, bbox.zmax);

  Vertex vertices[VERTEX_COUNT] = {
    backBottomLeft, backBottomRight, backTopRight, backTopLeft,
    frontBottomLeft, frontBottomRight, frontTopRight, frontTopLeft
  };

  CollisionInfo info;


  for (auto i(0u) ; i < VERTEX_COUNT ; ++i)
    {
      const Atom* nearest(nullptr);
      GLfloat shortestDistance(vertices[i].distance(std::begin(molecule.atoms)->position) + 10.f);

      for (const auto& atom : molecule.atoms)
        {
          const auto distance(vertices[i].distance(atom.position));

          if (distance <= atom.radius && distance <= shortestDistance)
            {
              shortestDistance = distance;
              nearest = &atom;

              info.edgeFlags = info.edgeFlags | (0x1 << i);
            }
        }

      if (nearest)
        {
          info.centers[i] = nearest->position;
          setColor(info.centers[i], nearest->name);
        }
    }

  return info;
}


int main(int argc, char** argv)
{
  if (!glfwInit())
    {
      std::cerr << "Failed to initialize GLFW" << std::endl;
      return -1;
    }

  auto xSubdivision(30u), ySubdivision(30u), zSubdivision(30u);

  if (argc > 2)
    {
      auto subCount = std::stoul(argv[2]);
      xSubdivision = ySubdivision = zSubdivision = subCount;

    }

  glfwWindowHint(GLFW_SAMPLES, 4);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

  auto winWidth(1024), winHeight(768);
  auto window = glfwCreateWindow(winWidth, winHeight, "GLFW", nullptr, nullptr);
  if (!window)
    {
      std::cerr << "Failed to open GLFW window." << std::endl;
      glfwTerminate();
      return -1;
    }

  glfwMakeContextCurrent(window);
  glewExperimental = true;

  auto err = glewInit();
  if (err != GLEW_OK)
    {
      std::cerr << "Failed to initialize glew: " << glewGetErrorString(err) << std::endl;
      glfwTerminate();
      return -1;
    }
  // GLEW seems to set an error flag, even if it's ok
  // This line purge the error
  glGetError();

  MoleculeLoader moleculeLoader;
  Molecule molecule(moleculeLoader.loadFromFile(argv[1]));
  auto bbox(molecule.getBBox());
  auto subBoxes(subDivideBBox(bbox, xSubdivision, ySubdivision, zSubdivision));

  std::vector<CollisionInfo> edgeFlags(subBoxes.size());
  std::transform(std::begin(subBoxes), std::end(subBoxes),
                 std::begin(edgeFlags),
                 [&molecule](const BBox3<GLfloat>& bbox) { return cubeEdgeFlagFromCollision(bbox, molecule); });

  std::vector<GLfloat> triangles;
  for (auto i(0u) ; i < edgeFlags.size() ; ++i)
    {
      std::vector<GLfloat> vertices = vMarchCube(edgeFlags[i], subBoxes[i]);
      std::copy(std::begin(vertices), std::end(vertices),
                std::back_inserter(triangles));
    }

  Cube cube(bbox);
  std::vector<Cube> subCubes;
  subCubes.reserve(subBoxes.size());
  std::for_each(std::begin(subBoxes), std::end(subBoxes),
                [&subCubes](const BBox3<GLfloat>& b) { subCubes.emplace_back(b); });

  VBO vboBBox(cube.buildVBO());
  // VBO subCubesVBO;
  // subCubesVBO.allocate(subCubes.size() * 3 * 2 * 6 * 3);
  // for (const auto& c : subCubes)
  //   {
  //     subCubesVBO.appendData(reinterpret_cast<const GLfloat*>(c.triangles.data()), 3 * 2 * 6 * 3);
  //   }
  // DataInfo subCubesDataInfo, subCubesDataInfoColors;
  // subCubesDataInfoColors.index = 1;
  // subCubesDataInfoColors.stride = 2 * 3 * sizeof(GLfloat);
  // subCubesDataInfoColors.pointer = reinterpret_cast<const GLvoid*>(3 * sizeof(GLfloat));
  // subCubesVBO.addDataInfo(subCubesDataInfo);
  // subCubesVBO.addDataInfo(subCubesDataInfoColors);

  VBO vboTriangles;
  vboTriangles.allocate(triangles.size() * 2);
  vboTriangles.appendData(triangles);

  auto triangleStride((3 * 3) * sizeof(GLfloat));
  DataInfo trianglesDataInfo;
  trianglesDataInfo.stride = triangleStride;
  vboTriangles.addDataInfo(trianglesDataInfo);

  DataInfo trianglesColorsDataInfo;
  trianglesColorsDataInfo.index = 1;
  trianglesColorsDataInfo.stride = triangleStride;
  trianglesColorsDataInfo.pointer = reinterpret_cast<const GLvoid*>(3 * sizeof(GLfloat));
  vboTriangles.addDataInfo(trianglesColorsDataInfo);

  DataInfo trianglesNormalDataInfo;
  trianglesNormalDataInfo.index = 2;
  trianglesNormalDataInfo.stride = triangleStride;
  trianglesNormalDataInfo.pointer = reinterpret_cast<const GLvoid*>((2 * 3) * sizeof(GLfloat));
  vboTriangles.addDataInfo(trianglesNormalDataInfo);

  VAO vao, vaoSubCubes, vaoTriangles;

  vao.beginSetup();
  vao.setupVBO(vboBBox);
  vao.endSetup();

  // vaoSubCubes.beginSetup();
  // vaoSubCubes.setupVBO(subCubesVBO);
  // vaoSubCubes.endSetup();

  vaoTriangles.beginSetup();
  vaoTriangles.setupVBO(vboTriangles);
  vaoTriangles.endSetup();

  Shader shader("shaderCube.vert", "shaderCube.frag"),
    lightShader("lightShader.vert", "lightShader.frag");

  auto fov(static_cast<GLfloat>(winWidth) / winHeight);
  auto projection = glm::perspective(glm::radians(75.f), fov, 0.1f, 1000.f);
  auto model = glm::mat4(1.f);

  auto matrixId = shader.getUniformLocation("MVP");
  auto matrixIdLight = lightShader.getUniformLocation("MVP");
  auto ambientLightIntensityId = lightShader.getUniformLocation("ambientLightIntensity");
  auto ambientLightColorId = lightShader.getUniformLocation("ambientLightColor");

  auto diffuseLightPositionId = lightShader.getUniformLocation("diffuseLightPosition");
  auto diffuseLightColorId = lightShader.getUniformLocation("diffuseLightColor");
  auto diffuseLightIntensityId = lightShader.getUniformLocation("diffuseLightIntensity");

  auto specularLightIntensityId = lightShader.getUniformLocation("specularLightIntensity");
  auto eyePositionId = lightShader.getUniformLocation("eyePosition");

  GLenum polygonMode(GL_FILL);
  auto enterPressBefore(false);
  auto mousePressedBefore(false);

  auto position = glm::vec3(bbox.xmin + (bbox.xmax - bbox.xmin) / 2.f,
                            bbox.ymin + (bbox.ymax - bbox.ymin) / 2.f,
                            50);
  auto target = glm::vec3(bbox.xmin + (bbox.xmax - bbox.xmin) / 2.f,
                          bbox.ymin + (bbox.ymax - bbox.ymin) / 2.f,
                          bbox.zmin + (bbox.zmax - bbox.zmin) / 2.f);
  auto forward = glm::vec3(0.f, 0.f, -1.f);
  auto up = glm::vec3(0.f, 1.f, 0.f);
  auto pitch(0.f), yaw(-90.f);
  auto speed(1.f);

  auto lastMouseX(winWidth / 2.0), lastMouseY(winHeight / 2.0);

  auto verticesCount(3 * 2 * 6);

  auto ambientLightColor = glm::vec3(1.f, 1.f, 1.f);
  auto ambientLightIntensity = .1f;

  auto diffuseLightColor = glm::vec3(.9f, .9f, .9f);
  auto diffuseLightPosition = glm::vec3(position.x+40.f, position.y+40.f, position.z + 100);
  auto diffuseLightIntensity = .8f;

  auto specularLightIntensity = .5f;

  // glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
  glfwSetWindowSizeCallback(window, windowSizeCallback);
  glfwSetInputMode(window, GLFW_STICKY_KEYS, 1);

  glClearColor(0.2f, 0.2f, 0.7f, 0.f);
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LESS);

  do {
    auto view = glm::lookAt(position,
                            position + forward,
                            up);

    auto mvp = projection * view * model;

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    shader.useProgram();
    shader.setUniformMat4(matrixId, mvp);

    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    vao.bind();
    glDrawArrays(GL_TRIANGLES, 0, verticesCount);

    // vaoSubCubes.bind();
    // glDrawArrays(GL_TRIANGLES, 0, verticesCount * subCubes.size());

    lightShader.useProgram();
    lightShader.setUniformMat4(matrixIdLight, mvp);
    lightShader.setUniformFloat(ambientLightIntensityId, ambientLightIntensity);
    lightShader.setUniformVec3(ambientLightColorId, ambientLightColor);

    lightShader.setUniformFloat(diffuseLightIntensityId, diffuseLightIntensity);
    lightShader.setUniformVec3(diffuseLightPositionId, diffuseLightPosition);
    lightShader.setUniformVec3(diffuseLightColorId, diffuseLightColor);

    lightShader.setUniformFloat(specularLightIntensityId, specularLightIntensity);
    lightShader.setUniformVec3(eyePositionId, position);

    glPolygonMode(GL_FRONT_AND_BACK, polygonMode);

    vaoTriangles.bind();
    glDrawArrays(GL_TRIANGLES, 0, triangles.size() / 9);

    Shader::resetUsedProgram();

    VAO::BindNull();

    glfwSwapBuffers(window);
    glfwPollEvents();

    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
      {
        position += speed * forward;
      }
    else if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
      {
        position -= speed * forward;
      }

    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
      {
        position -= speed * glm::normalize(glm::cross(forward, up));
      }
    else if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
      {
        position += speed * glm::normalize(glm::cross(forward, up));
      }

    auto mouseButtonState = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_1);
    if (mouseButtonState == GLFW_PRESS && !mousePressedBefore)
      {
        mousePressedBefore = true;
        glfwGetCursorPos(window, &lastMouseX, &lastMouseY);
      }
    else if (mouseButtonState == GLFW_PRESS && mousePressedBefore)
      {
        GLdouble xpos, ypos;
        glfwGetCursorPos(window, &xpos, &ypos);

        GLfloat xoffset = xpos - lastMouseX;
        GLfloat yoffset = lastMouseY - ypos; // Reversed since y-coordinates range from bottom to top
        lastMouseX = xpos;
        lastMouseY = ypos;

        GLfloat sensitivity = 0.05f;
        xoffset *= sensitivity;
        yoffset *= sensitivity;

        yaw += xoffset;
        pitch += yoffset;

        if(pitch > 89.0f)
          pitch =  89.0f;
        else if(pitch < -89.0f)
          pitch = -89.0f;

        forward.x = cos(glm::radians(pitch)) * cos(glm::radians(yaw));
        forward.y = sin(glm::radians(pitch));
        forward.z = cos(glm::radians(pitch)) * sin(glm::radians(yaw));
        forward = glm::normalize(forward);
      }
    else if (mouseButtonState == GLFW_RELEASE && mousePressedBefore)
      {
        mousePressedBefore = false;
      }

    auto enterKeyState = glfwGetKey(window, GLFW_KEY_ENTER);
    if (enterKeyState == GLFW_PRESS && !enterPressBefore)
      {
        enterPressBefore = true;
      }
    else if (enterKeyState == GLFW_RELEASE && enterPressBefore)
      {
        enterPressBefore = false;
        polygonMode = polygonMode == GL_FILL ? GL_LINE : GL_FILL;
      }
  } while (glfwGetKey(window, GLFW_KEY_ESCAPE) != GLFW_PRESS && !glfwWindowShouldClose(window));

glfwTerminate();

return 0;
}
