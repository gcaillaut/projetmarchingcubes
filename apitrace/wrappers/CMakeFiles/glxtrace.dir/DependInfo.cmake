# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/gaetan/Téléchargements/apitrace/wrappers/config.cpp" "/home/gaetan/Téléchargements/apitrace/build/wrappers/CMakeFiles/glxtrace.dir/config.cpp.o"
  "/home/gaetan/Téléchargements/apitrace/wrappers/dlsym.cpp" "/home/gaetan/Téléchargements/apitrace/build/wrappers/CMakeFiles/glxtrace.dir/dlsym.cpp.o"
  "/home/gaetan/Téléchargements/apitrace/wrappers/glcaps.cpp" "/home/gaetan/Téléchargements/apitrace/build/wrappers/CMakeFiles/glxtrace.dir/glcaps.cpp.o"
  "/home/gaetan/Téléchargements/apitrace/wrappers/gltrace_state.cpp" "/home/gaetan/Téléchargements/apitrace/build/wrappers/CMakeFiles/glxtrace.dir/gltrace_state.cpp.o"
  "/home/gaetan/Téléchargements/apitrace/build/wrappers/glxtrace.cpp" "/home/gaetan/Téléchargements/apitrace/build/wrappers/CMakeFiles/glxtrace.dir/glxtrace.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "HAVE_BACKTRACE=1"
  "HAVE_READPROC_H"
  "HAVE_X11"
  "_GNU_SOURCE"
  "__STDC_FORMAT_MACROS"
  "__STDC_LIMIT_MACROS"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "wrappers"
  "../wrappers"
  "../thirdparty/khronos"
  "../compat"
  "../thirdparty/snappy"
  "../thirdparty/libbacktrace"
  "../thirdparty/gtest/include"
  "../common"
  "../helpers"
  "dispatch"
  "../dispatch"
  "../guids"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/gaetan/Téléchargements/apitrace/build/helpers/CMakeFiles/glhelpers.dir/DependInfo.cmake"
  "/home/gaetan/Téléchargements/apitrace/build/dispatch/CMakeFiles/glproc_gl.dir/DependInfo.cmake"
  "/home/gaetan/Téléchargements/apitrace/build/wrappers/CMakeFiles/trace.dir/DependInfo.cmake"
  "/home/gaetan/Téléchargements/apitrace/build/common/CMakeFiles/common.dir/DependInfo.cmake"
  "/home/gaetan/Téléchargements/apitrace/build/thirdparty/libbacktrace/CMakeFiles/backtrace.dir/DependInfo.cmake"
  "/home/gaetan/Téléchargements/apitrace/build/guids/CMakeFiles/guids.dir/DependInfo.cmake"
  "/home/gaetan/Téléchargements/apitrace/build/thirdparty/snappy/CMakeFiles/snappy_bundled.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
