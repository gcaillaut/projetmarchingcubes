# Install script for directory: /home/gaetan/Téléchargements/apitrace/scripts

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "0")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/apitrace/scripts" TYPE PROGRAM FILES
    "/home/gaetan/Téléchargements/apitrace/scripts/retracediff.py"
    "/home/gaetan/Téléchargements/apitrace/scripts/snapdiff.py"
    "/home/gaetan/Téléchargements/apitrace/scripts/tracediff.py"
    "/home/gaetan/Téléchargements/apitrace/scripts/tracecheck.py"
    "/home/gaetan/Téléchargements/apitrace/scripts/jsonextractimages.py"
    "/home/gaetan/Téléchargements/apitrace/scripts/unpickle.py"
    "/home/gaetan/Téléchargements/apitrace/scripts/profileshader.py"
    "/home/gaetan/Téléchargements/apitrace/scripts/convert.py"
    "/home/gaetan/Téléchargements/apitrace/scripts/highlight.py"
    "/home/gaetan/Téléchargements/apitrace/scripts/leaks.py"
    "/home/gaetan/Téléchargements/apitrace/scripts/jsondiff.py"
    )
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/apitrace/scripts" TYPE FILE FILES "/home/gaetan/Téléchargements/apitrace/scripts/apitrace.PIXExp")
endif()

