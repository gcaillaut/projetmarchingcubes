# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/gaetan/Téléchargements/apitrace/common/highlight.cpp" "/home/gaetan/Téléchargements/apitrace/build/common/CMakeFiles/common.dir/highlight.cpp.o"
  "/home/gaetan/Téléchargements/apitrace/common/os_backtrace.cpp" "/home/gaetan/Téléchargements/apitrace/build/common/CMakeFiles/common.dir/os_backtrace.cpp.o"
  "/home/gaetan/Téléchargements/apitrace/common/os_crtdbg.cpp" "/home/gaetan/Téléchargements/apitrace/build/common/CMakeFiles/common.dir/os_crtdbg.cpp.o"
  "/home/gaetan/Téléchargements/apitrace/common/os_posix.cpp" "/home/gaetan/Téléchargements/apitrace/build/common/CMakeFiles/common.dir/os_posix.cpp.o"
  "/home/gaetan/Téléchargements/apitrace/common/trace_callset.cpp" "/home/gaetan/Téléchargements/apitrace/build/common/CMakeFiles/common.dir/trace_callset.cpp.o"
  "/home/gaetan/Téléchargements/apitrace/common/trace_dump.cpp" "/home/gaetan/Téléchargements/apitrace/build/common/CMakeFiles/common.dir/trace_dump.cpp.o"
  "/home/gaetan/Téléchargements/apitrace/common/trace_fast_callset.cpp" "/home/gaetan/Téléchargements/apitrace/build/common/CMakeFiles/common.dir/trace_fast_callset.cpp.o"
  "/home/gaetan/Téléchargements/apitrace/common/trace_file.cpp" "/home/gaetan/Téléchargements/apitrace/build/common/CMakeFiles/common.dir/trace_file.cpp.o"
  "/home/gaetan/Téléchargements/apitrace/common/trace_file_read.cpp" "/home/gaetan/Téléchargements/apitrace/build/common/CMakeFiles/common.dir/trace_file_read.cpp.o"
  "/home/gaetan/Téléchargements/apitrace/common/trace_file_snappy.cpp" "/home/gaetan/Téléchargements/apitrace/build/common/CMakeFiles/common.dir/trace_file_snappy.cpp.o"
  "/home/gaetan/Téléchargements/apitrace/common/trace_file_zlib.cpp" "/home/gaetan/Téléchargements/apitrace/build/common/CMakeFiles/common.dir/trace_file_zlib.cpp.o"
  "/home/gaetan/Téléchargements/apitrace/common/trace_model.cpp" "/home/gaetan/Téléchargements/apitrace/build/common/CMakeFiles/common.dir/trace_model.cpp.o"
  "/home/gaetan/Téléchargements/apitrace/common/trace_option.cpp" "/home/gaetan/Téléchargements/apitrace/build/common/CMakeFiles/common.dir/trace_option.cpp.o"
  "/home/gaetan/Téléchargements/apitrace/common/trace_ostream_snappy.cpp" "/home/gaetan/Téléchargements/apitrace/build/common/CMakeFiles/common.dir/trace_ostream_snappy.cpp.o"
  "/home/gaetan/Téléchargements/apitrace/common/trace_ostream_zlib.cpp" "/home/gaetan/Téléchargements/apitrace/build/common/CMakeFiles/common.dir/trace_ostream_zlib.cpp.o"
  "/home/gaetan/Téléchargements/apitrace/common/trace_parser.cpp" "/home/gaetan/Téléchargements/apitrace/build/common/CMakeFiles/common.dir/trace_parser.cpp.o"
  "/home/gaetan/Téléchargements/apitrace/common/trace_parser_flags.cpp" "/home/gaetan/Téléchargements/apitrace/build/common/CMakeFiles/common.dir/trace_parser_flags.cpp.o"
  "/home/gaetan/Téléchargements/apitrace/common/trace_parser_loop.cpp" "/home/gaetan/Téléchargements/apitrace/build/common/CMakeFiles/common.dir/trace_parser_loop.cpp.o"
  "/home/gaetan/Téléchargements/apitrace/common/trace_profiler.cpp" "/home/gaetan/Téléchargements/apitrace/build/common/CMakeFiles/common.dir/trace_profiler.cpp.o"
  "/home/gaetan/Téléchargements/apitrace/common/trace_writer.cpp" "/home/gaetan/Téléchargements/apitrace/build/common/CMakeFiles/common.dir/trace_writer.cpp.o"
  "/home/gaetan/Téléchargements/apitrace/common/trace_writer_local.cpp" "/home/gaetan/Téléchargements/apitrace/build/common/CMakeFiles/common.dir/trace_writer_local.cpp.o"
  "/home/gaetan/Téléchargements/apitrace/common/trace_writer_model.cpp" "/home/gaetan/Téléchargements/apitrace/build/common/CMakeFiles/common.dir/trace_writer_model.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "HAVE_BACKTRACE=1"
  "HAVE_READPROC_H"
  "HAVE_X11"
  "_GNU_SOURCE"
  "__STDC_FORMAT_MACROS"
  "__STDC_LIMIT_MACROS"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../thirdparty/khronos"
  "../compat"
  "../thirdparty/snappy"
  "../thirdparty/libbacktrace"
  "../thirdparty/gtest/include"
  "../common"
  "../guids"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/gaetan/Téléchargements/apitrace/build/guids/CMakeFiles/guids.dir/DependInfo.cmake"
  "/home/gaetan/Téléchargements/apitrace/build/thirdparty/libbacktrace/CMakeFiles/backtrace.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
