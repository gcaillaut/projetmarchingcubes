# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/gaetan/Téléchargements/apitrace/image/image_bmp.cpp" "/home/gaetan/Téléchargements/apitrace/build/image/CMakeFiles/image.dir/image_bmp.cpp.o"
  "/home/gaetan/Téléchargements/apitrace/image/image_md5.cpp" "/home/gaetan/Téléchargements/apitrace/build/image/CMakeFiles/image.dir/image_md5.cpp.o"
  "/home/gaetan/Téléchargements/apitrace/image/image_png.cpp" "/home/gaetan/Téléchargements/apitrace/build/image/CMakeFiles/image.dir/image_png.cpp.o"
  "/home/gaetan/Téléchargements/apitrace/image/image_pnm.cpp" "/home/gaetan/Téléchargements/apitrace/build/image/CMakeFiles/image.dir/image_pnm.cpp.o"
  "/home/gaetan/Téléchargements/apitrace/image/image_raw.cpp" "/home/gaetan/Téléchargements/apitrace/build/image/CMakeFiles/image.dir/image_raw.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "HAVE_BACKTRACE=1"
  "HAVE_READPROC_H"
  "HAVE_X11"
  "_GNU_SOURCE"
  "__STDC_FORMAT_MACROS"
  "__STDC_LIMIT_MACROS"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../thirdparty/khronos"
  "../compat"
  "../thirdparty/snappy"
  "../thirdparty/libbacktrace"
  "../thirdparty/gtest/include"
  "../common"
  "../thirdparty/md5"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/gaetan/Téléchargements/apitrace/build/thirdparty/md5/CMakeFiles/md5_bundled.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
