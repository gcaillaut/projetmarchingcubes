# CMake generated Testfile for 
# Source directory: /home/gaetan/Téléchargements/apitrace
# Build directory: /home/gaetan/Téléchargements/apitrace/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(thirdparty/snappy)
subdirs(thirdparty/libbacktrace)
subdirs(thirdparty/md5)
subdirs(guids)
subdirs(common)
subdirs(dispatch)
subdirs(helpers)
subdirs(wrappers)
subdirs(image)
subdirs(retrace)
subdirs(cli)
subdirs(scripts)
