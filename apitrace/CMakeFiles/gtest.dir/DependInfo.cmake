# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/gaetan/Téléchargements/apitrace/thirdparty/gtest/src/gtest-all.cc" "/home/gaetan/Téléchargements/apitrace/build/CMakeFiles/gtest.dir/thirdparty/gtest/src/gtest-all.cc.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "HAVE_BACKTRACE=1"
  "HAVE_READPROC_H"
  "HAVE_X11"
  "_GNU_SOURCE"
  "__STDC_FORMAT_MACROS"
  "__STDC_LIMIT_MACROS"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../thirdparty/khronos"
  "../compat"
  "../thirdparty/snappy"
  "../thirdparty/libbacktrace"
  "../thirdparty/gtest/include"
  "../thirdparty/gtest"
  "../common"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
