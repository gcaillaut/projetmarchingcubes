# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/gaetan/Téléchargements/apitrace/thirdparty/md5/md5.c" "/home/gaetan/Téléchargements/apitrace/build/thirdparty/md5/CMakeFiles/md5_bundled.dir/md5.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "HAVE_BACKTRACE=1"
  "HAVE_READPROC_H"
  "HAVE_X11"
  "_GNU_SOURCE"
  "__STDC_FORMAT_MACROS"
  "__STDC_LIMIT_MACROS"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../compat"
  "../thirdparty/snappy"
  "../thirdparty/libbacktrace"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
