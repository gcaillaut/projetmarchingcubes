# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/gaetan/Téléchargements/apitrace/thirdparty/snappy/snappy-c.cc" "/home/gaetan/Téléchargements/apitrace/build/thirdparty/snappy/CMakeFiles/snappy_bundled.dir/snappy-c.cc.o"
  "/home/gaetan/Téléchargements/apitrace/thirdparty/snappy/snappy-sinksource.cc" "/home/gaetan/Téléchargements/apitrace/build/thirdparty/snappy/CMakeFiles/snappy_bundled.dir/snappy-sinksource.cc.o"
  "/home/gaetan/Téléchargements/apitrace/thirdparty/snappy/snappy-stubs-internal.cc" "/home/gaetan/Téléchargements/apitrace/build/thirdparty/snappy/CMakeFiles/snappy_bundled.dir/snappy-stubs-internal.cc.o"
  "/home/gaetan/Téléchargements/apitrace/thirdparty/snappy/snappy.cc" "/home/gaetan/Téléchargements/apitrace/build/thirdparty/snappy/CMakeFiles/snappy_bundled.dir/snappy.cc.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "HAVE_CONFIG_H"
  "HAVE_READPROC_H"
  "HAVE_X11"
  "NDEBUG"
  "_GNU_SOURCE"
  "__STDC_FORMAT_MACROS"
  "__STDC_LIMIT_MACROS"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../compat"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
