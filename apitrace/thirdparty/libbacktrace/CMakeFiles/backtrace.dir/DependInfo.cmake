# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/gaetan/Téléchargements/apitrace/thirdparty/libbacktrace/atomic.c" "/home/gaetan/Téléchargements/apitrace/build/thirdparty/libbacktrace/CMakeFiles/backtrace.dir/atomic.c.o"
  "/home/gaetan/Téléchargements/apitrace/thirdparty/libbacktrace/backtrace.c" "/home/gaetan/Téléchargements/apitrace/build/thirdparty/libbacktrace/CMakeFiles/backtrace.dir/backtrace.c.o"
  "/home/gaetan/Téléchargements/apitrace/thirdparty/libbacktrace/dwarf.c" "/home/gaetan/Téléchargements/apitrace/build/thirdparty/libbacktrace/CMakeFiles/backtrace.dir/dwarf.c.o"
  "/home/gaetan/Téléchargements/apitrace/thirdparty/libbacktrace/elf.c" "/home/gaetan/Téléchargements/apitrace/build/thirdparty/libbacktrace/CMakeFiles/backtrace.dir/elf.c.o"
  "/home/gaetan/Téléchargements/apitrace/thirdparty/libbacktrace/fileline.c" "/home/gaetan/Téléchargements/apitrace/build/thirdparty/libbacktrace/CMakeFiles/backtrace.dir/fileline.c.o"
  "/home/gaetan/Téléchargements/apitrace/thirdparty/libbacktrace/mmap.c" "/home/gaetan/Téléchargements/apitrace/build/thirdparty/libbacktrace/CMakeFiles/backtrace.dir/mmap.c.o"
  "/home/gaetan/Téléchargements/apitrace/thirdparty/libbacktrace/mmapio.c" "/home/gaetan/Téléchargements/apitrace/build/thirdparty/libbacktrace/CMakeFiles/backtrace.dir/mmapio.c.o"
  "/home/gaetan/Téléchargements/apitrace/thirdparty/libbacktrace/posix.c" "/home/gaetan/Téléchargements/apitrace/build/thirdparty/libbacktrace/CMakeFiles/backtrace.dir/posix.c.o"
  "/home/gaetan/Téléchargements/apitrace/thirdparty/libbacktrace/print.c" "/home/gaetan/Téléchargements/apitrace/build/thirdparty/libbacktrace/CMakeFiles/backtrace.dir/print.c.o"
  "/home/gaetan/Téléchargements/apitrace/thirdparty/libbacktrace/simple.c" "/home/gaetan/Téléchargements/apitrace/build/thirdparty/libbacktrace/CMakeFiles/backtrace.dir/simple.c.o"
  "/home/gaetan/Téléchargements/apitrace/thirdparty/libbacktrace/sort.c" "/home/gaetan/Téléchargements/apitrace/build/thirdparty/libbacktrace/CMakeFiles/backtrace.dir/sort.c.o"
  "/home/gaetan/Téléchargements/apitrace/thirdparty/libbacktrace/state.c" "/home/gaetan/Téléchargements/apitrace/build/thirdparty/libbacktrace/CMakeFiles/backtrace.dir/state.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "HAVE_READPROC_H"
  "HAVE_X11"
  "_GNU_SOURCE"
  "__STDC_FORMAT_MACROS"
  "__STDC_LIMIT_MACROS"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "thirdparty/libbacktrace"
  "../thirdparty/libbacktrace"
  "../compat"
  "../thirdparty/snappy"
  "../thirdparty/libbacktrace/auxincl"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
