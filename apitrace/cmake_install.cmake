# Install script for directory: /home/gaetan/Téléchargements/apitrace

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "0")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/doc/apitrace" TYPE FILE FILES
    "/home/gaetan/Téléchargements/apitrace/README.markdown"
    "/home/gaetan/Téléchargements/apitrace/docs/BUGS.markdown"
    "/home/gaetan/Téléchargements/apitrace/docs/NEWS.markdown"
    "/home/gaetan/Téléchargements/apitrace/docs/USAGE.markdown"
    )
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/doc/apitrace" TYPE FILE RENAME "LICENSE.txt" FILES "/home/gaetan/Téléchargements/apitrace/LICENSE")
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("/home/gaetan/Téléchargements/apitrace/build/thirdparty/snappy/cmake_install.cmake")
  include("/home/gaetan/Téléchargements/apitrace/build/thirdparty/libbacktrace/cmake_install.cmake")
  include("/home/gaetan/Téléchargements/apitrace/build/thirdparty/md5/cmake_install.cmake")
  include("/home/gaetan/Téléchargements/apitrace/build/guids/cmake_install.cmake")
  include("/home/gaetan/Téléchargements/apitrace/build/common/cmake_install.cmake")
  include("/home/gaetan/Téléchargements/apitrace/build/dispatch/cmake_install.cmake")
  include("/home/gaetan/Téléchargements/apitrace/build/helpers/cmake_install.cmake")
  include("/home/gaetan/Téléchargements/apitrace/build/wrappers/cmake_install.cmake")
  include("/home/gaetan/Téléchargements/apitrace/build/image/cmake_install.cmake")
  include("/home/gaetan/Téléchargements/apitrace/build/retrace/cmake_install.cmake")
  include("/home/gaetan/Téléchargements/apitrace/build/cli/cmake_install.cmake")
  include("/home/gaetan/Téléchargements/apitrace/build/scripts/cmake_install.cmake")

endif()

if(CMAKE_INSTALL_COMPONENT)
  set(CMAKE_INSTALL_MANIFEST "install_manifest_${CMAKE_INSTALL_COMPONENT}.txt")
else()
  set(CMAKE_INSTALL_MANIFEST "install_manifest.txt")
endif()

string(REPLACE ";" "\n" CMAKE_INSTALL_MANIFEST_CONTENT
       "${CMAKE_INSTALL_MANIFEST_FILES}")
file(WRITE "/home/gaetan/Téléchargements/apitrace/build/${CMAKE_INSTALL_MANIFEST}"
     "${CMAKE_INSTALL_MANIFEST_CONTENT}")
