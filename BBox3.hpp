#ifndef _BBOX_H_
#define _BBOX_H_

#include <stdexcept>


template<typename T>
class BBox3
{
public:
  BBox3() = default;
  BBox3(T xmin, T xmax, T ymin, T ymax, T zmin, T zmax)
    : xmin(xmin), xmax(xmax),
      ymin(ymin), ymax(ymax),
      zmin(zmin), zmax(zmax)
  {
    if (xmax < xmin || ymax < ymin || zmax < zmin)
      {
        throw std::logic_error("BBox has an upper bound greater than its lower bound");
      }
  }

  T getWidth() const
  {
    return xmax - xmin;
  }

  T getHeight() const
  {
    return ymax - ymin;
  }

  T getDepth() const
  {
    return zmax - zmin;
  }

public:
  T xmin, xmax, ymin, ymax, zmin, zmax;
};

#endif /* _BBOX_H_ */
